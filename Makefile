# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: mrudloff <mrudloff@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2023/07/26 16:11:51 by mrudloff          #+#    #+#              #
#    Updated: 2023/07/27 16:43:13 by mrudloff         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

#PROJECT
NAME	=	tournament	

#COMMAND
CC		=	@clang
CFLAGS	=	-g -Wall -Wextra -Werror
IFLAGS	=	-Iinc -Ilibs/printf/include
MK		=	@mkdir -p
RM		=	@rm -f
HARDRM	=	@rm -rf
CLEAR	=	@clear
#FILE	=	$(shell ls -l src/**/* | grep -F .c | wc -l)
CMP		=	1

# COLOR #
GREEN	=	\033[38;5;76m
RED		=	\033[38;5;124m
YELLOW	=	\033[38;5;226m
ORANGE	=	\033[38;5;202m
PURPLE	=	\033[38;5;213m
LBLUE	=	\033[38;5;51m
BLUE	=	\033[38;5;117m
INDI	=	\033[38;5;99m
RESET	=	\033[00m

#LIBFT
PRINTF_NAME	=	./libs/printf/libftprintf.a
PRINTF_DIR	=	./libs/printf/ --no-print-directory

#SOURCES_MANDATORY
SRCS		:=	main.c openfile.c readfile.c
SRCS_DIR	:=	src
SRCS		:=	$(SRCS:%=$(SRCS_DIR)/%)

#OBJECTS
OBJS_DIR	:=	.objs
OBJS		:=	$(addprefix $(OBJS_DIR)/, $(SRCS:%.c=%.o))

all : $(PRINTF_NAME) $(NAME)

bonus : $(PRINTF_NAME) $(NAME_B)

$(OBJS_DIR)/%.o: %.c
	$(MK) $(@D)
	@printf "\r$(LBLUE)[$(RESET)+$(LBLUE)] $(RESET)Compiling $(GREEN)$<$(BLUE) [$(RESET)$(CMP)$(BLUE)/$(RESET)$(FILE)$(BLUE)]$(RESET)         \r"
	$(CC) $(CFLAGS) $(IFLAGS) -c $< -o $@
	@$(eval CMP=$(shell echo $$(($(CMP)+1))))

$(PRINTF_NAME):
	@make -C $(PRINTF_DIR)

$(NAME) : $(OBJS)
	$(CC) $(CFLAGS) $(IFLAGS) $(OBJS) $(PRINTF_NAME) -o $(NAME)
	@printf "\r"

clean :
	@make -C $(PRINTF_DIR) clean
	@rm -f $(OBJS_MLX)
	$(HARDRM) $(OBJS_DIR)

fclean : clean
	@make fclean -C $(PRINTF_DIR)
	$(RM) $(NAME)

re : fclean all

run: all
	valgrind --leak-check=full --show-leak-kinds=all ./$(NAME) test.csv

run2: all
	valgrind --leak-check=full --show-leak-kinds=all ./$(NAME) test1.csv

.PHONY :
	all clean fclean re
