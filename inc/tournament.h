/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tournament.h                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mrudloff <mrudloff@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/07/26 16:15:02 by mrudloff          #+#    #+#             */
/*   Updated: 2023/07/27 17:29:15 by mrudloff         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef TOURNAMENT_H
# define TOURNAMENT_H

# include <stdio.h>
# include <string.h>
# include <unistd.h>
# include <stdlib.h>

FILE	*ft_load_csv(const char *av);
char	**ft_dump_file(FILE *filePointer);

#endif
