/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   openfile.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mrudloff <mrudloff@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/07/26 16:27:06 by mrudloff          #+#    #+#             */
/*   Updated: 2023/07/27 16:39:30 by mrudloff         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <fcntl.h>
#include <stdio.h>
#include <sys/stat.h>
#include "../libs/printf/include/ft_printf.h"
#include "../inc/tournament.h"

static int	ft_check_extension(const char *str, const char *ext){
	int		size;
	int		ext_size;
	char	*file_ext;

	if (!str || !ext)
		return (0);
	size = ft_strlen(str);
	ext_size = ft_strlen(ext);
	file_ext = ft_substr(str, (size - ext_size), ext_size);
	size = ft_strcmp(ext, file_ext);
	free (file_ext);
	return (size);
}

FILE	*ft_load_csv(const char *av){

	FILE	*filePointer;

	filePointer = NULL;
	if (!av)
		return (filePointer);
	if (access(av, R_OK) == -1){
		ft_putstr("Error: ");
		perror(av);
		return (filePointer);
	}
	if (ft_check_extension(av, ".csv"))
		return (filePointer);
	filePointer = fopen(av, "r");
	if (filePointer == NULL){
		ft_printf("Error: ", av);
		perror(av);
		return (NULL);
	}
	return (filePointer);
}
