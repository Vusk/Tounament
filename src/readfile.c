/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   readfile.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mrudloff <mrudloff@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/07/27 13:38:22 by mrudloff          #+#    #+#             */
/*   Updated: 2023/07/29 01:25:29 by mrudloff         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/tournament.h"
#include "../libs/printf/include/ft_printf.h"
#include <stdio.h>
#include <stdlib.h>
#define MAX_LINES 1000
#define MAX_LINE_LENGTH 256

char **ft_dump_file(FILE *filePointer){
	int		rtn;
	char	**tab;
	char	buffer[MAX_LINE_LENGTH];
	char	*line;

	line = NULL;
	rtn = 0;
	tab = NULL;
	if (!filePointer)
		return (NULL);
	tab = (char **)malloc(sizeof(char *) * MAX_LINES);
	if (!tab){
		ft_putstr("Error: malloc failed\n");
		fclose(filePointer);
		return (NULL);
	}
	while (fgets(buffer, sizeof(buffer), filePointer) != NULL){
		tab[rtn] = ft_strdup(buffer);
		if (tab[rtn] == NULL){
			ft_putstr("Error: malloc failed\n");
			fclose(filePointer);
			free_split(tab);
			return (NULL);
		}
		if (rtn >= MAX_LINES){
			ft_putstr("Error: Can't read more lines\n");
			free(tab[rtn]);
			break;
		}
		rtn++;
		fclose(filePointer);
	}
	return (tab);
}
