/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mrudloff <mrudloff@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/07/26 16:04:14 by mrudloff          #+#    #+#             */
/*   Updated: 2023/07/29 01:25:30 by mrudloff         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"
#include "tournament.h"

/*static int	ft_print_tab(char **dump_file){
	int	i;

	i = 0;
	if (!dump_file)
		return (-1);
	while (dump_file && dump_file[i]){
		ft_putstr(dump_file[i]);
		i++;
	}
	return (i);
}*/

int	main(int ac, char *const *av){
	FILE	*filePointer;
	char	**dump_file;

	filePointer = NULL;
	dump_file = NULL;
	if (ac != 2){
		ft_putstr_fd("Error: Must have a .csv in parameter", 2);
		return (0);
	}
	filePointer = ft_load_csv(av[1]);
	if (filePointer == NULL)
		return (0);
	dump_file = ft_dump_file(filePointer);
	if (!dump_file)
		return (0);
/*	if (dump_file && ft_print_tab(dump_file) < 0)
		ft_putstr_fd("Error: Nothing has been print\n", 2);*/
	if (dump_file && (free_split(dump_file) == 0)){
		ft_putstr_fd("Error: Empty\n", 2);
		return (-1);
	}
	return (0);
}
